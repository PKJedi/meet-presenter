# Meet Presenter

A chrome extension for Google Meet that makes it impossible for you to get muted.

Intended for use by teachers and other presenters who might get griefed by anonymous users muting them in Meet.

