const tabStates = {};

chrome.runtime.onInstalled.addListener(function () {
  chrome.declarativeContent.onPageChanged.removeRules(undefined, function () {
    chrome.declarativeContent.onPageChanged.addRules([
      {
        conditions: [
          new chrome.declarativeContent.PageStateMatcher({
            pageUrl: { urlPrefix: "https://meet.google.com/" },
          }),
        ],
        actions: [new chrome.declarativeContent.ShowPageAction()],
      },
    ]);
  });
});

chrome.pageAction.onClicked.addListener((tab) => {
  if (tabStates[tab.id] === undefined) {
    tabStates[tab.id] = false;
  }
  tabStates[tab.id] = !tabStates[tab.id];
  chrome.tabs.sendMessage(tab.id, {
    command: "set_is_presenter",
    value: tabStates[tab.id],
  });
  chrome.pageAction.setTitle({
    tabId: tab.id,
    title: (tabStates[tab.id] && "Presenting") || "Not presenting",
  });
  chrome.pageAction.setIcon({
    tabId: tab.id,
    path: {
      "32":
        (tabStates[tab.id] && "assets/icon_on_32.png") ||
        "assets/icon_off_32.png",
    },
  });
});
