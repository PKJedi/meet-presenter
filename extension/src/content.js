var isPresenter = false;

const muteButtonSelector =
  'div[role="button"][aria-label*="microphone"][data-is-muted]';

const togglePauseEvent = new KeyboardEvent("keydown", {
  key: "d",
  code: "KeyD",
  metaKey: true,
  charCode: 100,
  keyCode: 100,
  which: 100,
});

const waitUntilElementExists = (selector) =>
  new Promise((resolve, reject) => {
    const observer = new MutationObserver(function (mutations, observer) {
      const element = document.querySelector(selector);
      if (element) {
        observer.disconnect();
        resolve(element);
      }
    });

    observer.observe(document, {
      childList: true,
      subtree: true,
    });
  });

function watchMuteState(element) {
  const observer = new MutationObserver((mutations) => {
    const isMuted = mutations[0].target.getAttribute("data-is-muted") == "true";

    if (isMuted && isPresenter) {
      document.dispatchEvent(togglePauseEvent);
    }
  });
  observer.observe(element, {
    attributes: true,
    attributeFilter: ["data-is-muted"],
  });
  return observer;
}

const watchAndWaitForNext = ([element, observer]) => {
  if (observer) {
    observer.disconnect();
  }
  if (element) {
    observer = watchMuteState(element);
  }
  waitUntilElementExists(muteButtonSelector)
    .then((element) => [element, observer])
    .then(watchAndWaitForNext);
};

watchAndWaitForNext([null, null]);

chrome.runtime.onMessage.addListener((request, sender, sendResponse) => {
  if (request && request.command && request.command === "set_is_presenter") {
    isPresenter = request.value;
  }
});
